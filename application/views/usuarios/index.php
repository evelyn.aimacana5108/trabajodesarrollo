<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<br>
<h1 class="text-center">GESTION DE USUARIOS</h1>
<center>
  <button type="button" name="button" class="btn btn-primary"
   onclick="cargarListadoUsuarios();" title="Actualizar">
   <i class="fa fa-refresh"></i>
  </button>
   <!-- Trigger the modal with a button -->
   <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#modalNuevoUsuario">
     <i class="fa fa-plus-circle"></i>  Agregar Usuario
   </button>
 </center>
<div class="row">
  <div class="col-md-12"
  id="contenedor-listado-usuarios">
    <i class="fa fa-spin fa-lg fa-spinner"></i>
    Consultando Datos
  </div>
</div>


 <!-- Modal -->
 <div id="modalNuevoUsuario" style="z-index:9999 !important;" class="modal fade" role="dialog">
   <div class="modal-dialog modal-lg">

     <!-- Modal content-->
     <div class="modal-content">
       <div class="modal-header">
          <h4 class="modal-title"><i class="fa fa_users"></i>Nuevo Usuario</h4>
         <button type="button" class="close" data-dismiss="modal">&times;</button>
       </div>
       <div class="modal-body">
<form class="" action="<?php echo site_url("usuarios/insertarUsuario"); ?>" method="post" id="frm_nuevo_usuario">
  <div class="input-group form-group mt-3">
  <label for="">Apellido:    </label>
  <input type="text" class="form-control text-center p-3"
  placeholder="Apellido" name="apellido_usu" id="apellido_usu" required>
  </div>
        <br>
  <div class="form-row">
    <div class="input-group form-group mt-6">
    <label for="">Nombre:    </label><br>
    <input type="text" class="form-control text-center p-6"
    placeholder="Nombre" name="nombre_usu" id="nombre_usu" required>
    </div>  <br>
    <div class="input-group form-group mt-3">
    <label for="">Email:</label>
    <input type="email" class="form-control text-center p-3"
    placeholder="Email" name="email_usu" id="email_usu" required>
    </div>
  </div>
  <div class="card-body mt-3">
    <div class="input-group form-group mt-3">
    <label for="">CONTRASEÑA:</label>
    <input type="password" class="form-control text-center p-3"
    placeholder="Contraseña" name="password_usu" id="password_usu" required>
    <br>
    </div>
    <div class="card-body mt-3">
      <label for="">CONFIRME CONTRASEÑA:</label>
      <input type="password" class="form-control text-center p-3"
      placeholder="Contraseña" name="password_confirmada" id="password_confirmada" required>
    </div>
    <div class="input-group form-group mt-2">
    <label for="">Perfil:</label>
    <input type="" class="form-control text-center p-2"
    placeholder="Perfil" name="perfil_usu" id="perfil_usu" required>
    </div>
          <br>
    <div class="input-group form-group mt-2">
    <label for="">Estado:</label>
    <select class="form-control text-center p-2" type="text" name="estado_usu" id="estado_usu" required>
    <option value="">--Estado--</option>
    <option value="ACTIVO">ACTIVO</option>
    <option value="INACTIVO">INACTIVO</option>
  </select>
    </div>  <br>
  </div>
  <button type="submit" name="button" class="btn btn-succes"><i class="fa fa-save">Guardar</i> </button>
       </div>
       <div class="modal-footer">
         <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
       </div>
     </div>

   </div>
 </div>

 <!-- segunda ventana modal -->


  <!-- Modal -->
  <div id="modalEditarUsuario" style="z-index:9999 !important;" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
           <h4 class="modal-title"><i class="fa fa_users"></i>Editar Usuario</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body" id="contenedor-formulario-editar">
          <!-- UTILIZACION DE AJAX -->
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        </div>
      </div>

    </div>
  </div>

 <!-- fin de ventana MODAL DOS -->
 <script type="text/javascript">
     function cargarListadoUsuarios(){
       $("#contenedor-listado-usuarios")
       .html('<i class="fa fa-spin fa-lg fa-spinner"></i>');
       $("#contenedor-listado-usuarios")
       .load("<?php echo site_url(); ?>/usuarios/listado");
     }
     cargarListadoUsuarios();
     $("#frm_nuevo_usuario").validate({
       rules:{
         apellido_usu:{
           required:true,
         },
         password_usu:{
           required:true
         },
         password_confirmada:{
           required:true,
           equalTo:"#password_usu"
         }
       },
       messages:{
         apellido_usu:{
           required:"Por favor ingrese el apellido",
         }
       },
       submitHandler:function(form){//funcion para peticiones AJAX
           $.ajax({
             url:$(form).prop("action"),
             type:"post",
             data:$(form).serialize(),
             success:function(data){
                 cargarListadoUsuarios();
                 $("#modalNuevoUsuario").modal("hide");
                 $('body').removeClass('modal-open');//eliminamos la clase del body para poder hacer scroll
                 $('.modal-backdrop').remove();//eliminamos el backdrop del modal
                 var objetoJson=JSON.parse(data);
                 if(objetoJson.respuesta=="ok" || objetoJson.respuesta=="OK"){
                   iziToast.success({
                        title: 'CONFIRMACIÓN',
                        message: 'Inserción Exitosa',
                        position: 'topRight',
                      });
                 }else{
                   iziToast.error({
                        title: 'ERROR',
                        message: 'Error al procesar',
                        position: 'topRight',
                      });
                 }
             }
           });
       }
     });

 </script>


 <script type="text/javascript">
     function abrirFormularioEditar(id_usu){
       // alert("ok...");
       $("#contenedor-formulario-editar")
       .html('<i class="fa fa-spin fa-lg fa-spinner"></i>');
       $("#contenedor-formulario-editar")
       .load("<?php echo site_url('usuarios/editar'); ?>/"+id_usu);
       $("#modalEditarUsuario").modal("show");
     }
 </script>
