<?php if ($listadoUsuarios): ?>

  <table class="table table-bordered table-striped table-hover">
      <thead>
         <tr>
           <th class="text-center">ID</th>
           <th class="text-center">APELLIDO</th>
           <th class="text-center">NOMBRE</th>
           <th class="text-center">EMAIL</th>
           <th class="text-center">PERFIL</th>
           <th class="text-center">ESTADO</th>
           <!-- <th class="text-center">FECHA</th> -->
           <th class="text-center">ACCIONES</th>
         </tr>
      </thead>
      <tbody>
          <?php foreach ($listadoUsuarios->result() as $usuario): ?>
              <tr>
                  <td class="text-center">
                    <?php echo $usuario->id_usu; ?>
                  </td>
                  <td class="text-center">
                    <?php echo $usuario->apellido_usu; ?>
                  </td>
                  <td class="text-center">
                    <?php echo $usuario->nombre_usu; ?>
                  </td>
                  <td class="text-center">
                    <?php echo $usuario->email_usu; ?>
                  </td>
                  <td class="text-center">
                    <?php echo $usuario->perfil_usu; ?>
                  </td>
                  <td class="text-center">
                    <?php if ($usuario->estado_usu=="ACTIVO"): ?>
                      <div class="alert alert-success">
                        <?php echo $usuario->estado_usu;?>
                      </div>

                    <?php else: ?>
                      <div class="alert alert-danger">
                        <?php echo $usuario->estado_usu;?>
                      </div>

                    <?php endif; ?>
                  </td>
                  <!-- <td class="text-center">
                    <?php echo $usuario->fecha_creacion_usu; ?>
                  </td> -->
                  <td class="text-center">
                    <a href="#" onclick="abrirFormularioEditar(<?php echo $usuario->id_usu; ?>);" class="btn btn-warning">
                      <i class="fa fa-edit"></i>
                    </a>
                    &nbsp;
                    <a href="#" class="btn btn-danger" onclick="eliminarUsuario(<?php echo $usuario->id_usu; ?>);">
                      <i class="fa fa-trash"></i>
                    </a>
                  </td>
              </tr>
          <?php endforeach; ?>
      </tbody>
  </table>
<?php else: ?>
    <br>
    <div class="alert alert-danger">
        No se encontraron Usuarios Registrados
    </div>
<?php endif; ?>
  <script type="text/javascript">
    function eliminarUsuario(id_usu){

      iziToast.question({
      timeout: 20000,
      close: false,
      overlay: true,
      displayMode: 'once',
      id: 'question',
      zindex: 999,
      title: 'Hey',
      message: 'Are you sure about that?',
      position: 'center',
      buttons: [
                  ['<button><b>YES</b></button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                      $.ajax({
                          url:"<?php echo site_url('usuarios/eliminarUsuario'); ?>",
                          type:"post",
                          data:{"id_usu":id_usu},
                          success:function(data){
                            cargarListadoUsuarios();
                            alert(data);
                          }
                      });

                  }, true],
                  ['<button>NO</button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                  }],
              ],
              onClosing: function(instance, toast, closedBy){
                  console.info('Closing | closedBy: ' + closedBy);
              },
              onClosed: function(instance, toast, closedBy){
                  console.info('Closed | closedBy: ' + closedBy);
              }
          });


    }
  </script>



<script type="text/javascript">
$(document).ready(function() {
  $('#tbl-usuarios').DataTable( {
    dom: 'Blfrtip',
    buttons: [
        'copyHtml5',
        'excelHtml5',
        'csvHtml5',
        'pdfHtml5'
    ],
      "order": [[ 3, "desc" ]],
    language: {
      "decimal":        "",
  "emptyTable":     "No hay datos",
  "info":           "Mostrando START a END de TOTAL registros",
  "infoEmpty":      "Mostrando 0 a 0 de 0 registros",
  "infoFiltered":   "(Filtro de MAX total registros)",
  "infoPostFix":    "",
  "thousands":      ",",
  "lengthMenu":     "Mostrar MENU registros",
  "loadingRecords": "Cargando...",
  "processing":     "Procesando...",
  "search":         "Buscar:",
  "zeroRecords":    "No se encontraron coincidencias",
  "paginate": {
      "first":      "Primero",
      "last":       "Ultimo",
      "next":       "Próximo",
      "previous":   "Anterior"
  },
  "aria": {
      "sortAscending":  ": Activar orden de columna ascendente",
      "sortDescending": ": Activar orden de columna desendente"
  }
    }
  } );
} );

</script>
