
<form class="" action="<?php echo site_url("usuarios/actualizarUsuarioAjax"); ?>" method="post" id="frm_actualizar_usuario">
  <div class="input-group form-group mt-3">
    <input type="hidden" name="id_usu" id="id_usu"
  value="<?php echo $usuario->id_usu; ?>">
  <label for="">Apellido:    </label>
  <input type="text" class="form-control text-center p-3" value="<?php echo $usuario->apellido_usu; ?>"
  placeholder="Apellido" name="apellido_usu" id="apellido_usu" required>
  </div>
        <br>
  <div class="form-row">
    <div class="input-group form-group mt-6">
    <label for="">Nombre:    </label><br>
    <input type="text" class="form-control text-center p-6" value="<?php echo $usuario->nombre_usu; ?>"
    placeholder="Nombre" name="nombre_usu" id="nombre_usu" required>
    </div>  <br>
    <div class="input-group form-group mt-3">
    <label for="">Email:</label>
    <input type="email" class="form-control text-center p-3" value="<?php echo $usuario->email_usu; ?>"
    placeholder="Email" name="email_usu" id="email_usu" required>
    </div>
  </div>
          <br>
    <div class="input-group form-group mt-2">
      <label for="">PERFIL:</label>
      <br>
      <select class="form-control" name="perfil_usu"
      id="perfil_usu_editar">
       <option value="">Seleccione una opción</option>
       <option value="ADMINISTRADOR">ADMINISTRADOR</option>
       <option value="VENDEDOR">VENDEDOR</option>
      </select>
      <script type="text/javascript">
          $("#perfil_usu_editar").val("<?php echo $usuario->perfil_usu; ?>");
      </script>
    </div>  <br>
    <!-- <div class="input-group form-group mt-2">
    <label for="">Fecha:</label>
    <input type="date" class="form-control text-center p-2" value="<?php echo $usuario->fecha_creacion_usu; ?>"
    placeholder="FECHA" name="fecha_creacion_usu" id="fecha_creacion_usu" required>
    </div> -->
  </div>
  <button type="button" onclick="actualizar();" name="button"
class="btn btn-success">
  <i class="fa fa-pen"></i> Actualizar
</button>
       </div>
     </form>
     <script type="text/javascript">
     // Activado el pais seleccionado para el cliente
     // $("#perfil_usu_editar").val("<?php  echo $usuario->perfil_usu; ?>");
     $("#estado_usu").val("<?php  echo $usuario->estado_usu; ?>");

     </script>
     <script type="text/javascript">

     function actualizar(){

         $.ajax({
           url:$("#frm_actualizar_usuario").prop("action"),
           data:$("#frm_actualizar_usuario").serialize(),
           type:"post",
           success:function(data){
             cargarListadoUsuarios();
             $("#modalEditarUsuario").modal("hide");
             $('body').removeClass('modal-open');//eliminamos la clase del body para poder hacer scroll
             $('.modal-backdrop').remove();//eliminamos el backdrop del modal
             var objetoJson=JSON.parse(data);
             if(objetoJson.respuesta=="ok" || objetoJson.respuesta=="OK"){
               iziToast.success({
                    title: 'CONFIRMACIÓN',
                    message: 'Actualización Exitosa',
                    position: 'topRight',
                  });
             }else{
               iziToast.error({
                    title: 'ERROR',
                    message: 'Error al procesar',
                    position: 'topRight',
                  });
             }

           }
         });
     }

     </script>
