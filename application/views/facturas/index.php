  <br>
  <center>
    <ul class="nav nav-tabs">
      <li class="nav-item">
        <a class="nav-link active" aria-current="page" href="<?php echo site_url('facturas/index'); ?>">LISTADO DE FACTURAS</a>
      </li>
      <li class="nav-item">
        <a class="nav-link active" aria-current="page" href="<?php echo site_url('facturas/nuevo'); ?>">AGREGAR NUEVA FACTURA</a>
      </li>
    </ul>
  </center>
  <hr>
  <?php if ($listadoFacturas): ?>
    <table class="table table-bordered table-striped table-hover" id="tbl-facturas">
      <thead class="table-dark">
        <tr>
          <th class="text-center">ID</th>
          <th class="text-center">FECHA DE FACTURA</th>
          <th class="text-center">NOMBRE CLIENTE</th>
          <th class="text-center">OPCIONES</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($listadoClientes->result() as $filaTemporal): ?>
          <tr>
            <td class="text-center"><?php echo $filaTemporal->id_cli;?></td>
              <th><?php echo $filaTemporal->nombre_cli; ?></th>
            <td class="text-center">
                <a href="<?php echo site_url(); ?>/Facturas/editar/<?php echo $filaTemporal->id_cli;?>"class="btn btn-warning"><i class="fa fa-pen"></i></a>
                <?php if ($this->session->userdata("c0nectadoUSU")->perfil_usu=="ADMINISTRADOR"): ?>
                  <a onclick="confirmarEliminacion('<?php echo $filaTemporal->id_cli; ?>')" href="javascript:void(0)" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                <?php else: ?>

                <?php endif; ?>

              <!-- return confirm('¿Estas seguro de eliminar'); -->
                <!-- <?php echo site_url(); ?>/clientes/procesarEliminacion/<?php echo $filaTemporal->id_cli; ?> -->


              </td>
                      </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              <?php else: ?>
                <div class="alert alert-danger">
                  <h3>No se encontraron clientes resgistrados</h3>
                </div>
              <?php endif; ?>

              <script type="text/javascript">
                  function confirmarEliminacion(id_cli){
                        iziToast.question({
                            timeout: 20000,
                            close: false,
                            overlay: true,
                            displayMode: 'once',
                            id: 'question',
                            zindex: 999,
                            title: 'CONFIRMACIÓN',
                            message: '¿Esta seguro de eliminar el cliente de forma pernante?',
                            position: 'center',
                            buttons: [
                                ['<button><b>SI</b></button>', function (instance, toast) {

                                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                                    window.location.href=
                                    "<?php echo site_url(); ?>/clientes/procesarEliminacion/"+id_cli;

                                }, true],
                                ['<button>NO</button>', function (instance, toast) {

                                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                                }],
                            ]
                        });
                  }
              </script>
              <script type="text/javascript">
            $(document).ready(function() {
                $('#tbl-clientes').DataTable( {
                  dom: 'Blfrtip',
                  buttons: [
                      'copyHtml5',
                      'excelHtml5',
                      'csvHtml5',
                      'pdfHtml5'
                  ],
                    "order": [[ 3, "desc" ]],
                  language: {
                    "decimal":        "",
                "emptyTable":     "No hay datos",
                "info":           "Mostrando START a END de TOTAL registros",
                "infoEmpty":      "Mostrando 0 a 0 de 0 registros",
                "infoFiltered":   "(Filtro de MAX total registros)",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     "Mostrar MENU registros",
                "loadingRecords": "Cargando...",
                "processing":     "Procesando...",
                "search":         "Buscar:",
                "zeroRecords":    "No se encontraron coincidencias",
                "paginate": {
                    "first":      "Primero",
                    "last":       "Ultimo",
                    "next":       "Próximo",
                    "previous":   "Anterior"
                },
                "aria": {
                    "sortAscending":  ": Activar orden de columna ascendente",
                    "sortDescending": ": Activar orden de columna desendente"
                }
                  }
                } );
            } );

            </script>
