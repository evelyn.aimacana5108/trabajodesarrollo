<br>
<center>
  <ul class="nav nav-tabs">
    <li class="nav-item">
      <a class="nav-link active" aria-current="page" href="<?php echo site_url('clientes/index'); ?>">LISTADO DE CLIENTES</a>
    </li>
    <li class="nav-item">
      <a class="nav-link active" aria-current="page" href="<?php echo site_url('clientes/nuevo'); ?>">AGREGAR NUEVO CLIENTE</a>
    </li>
  </ul>
</center>
<br>
<form  action="<?php echo site_url(); ?>/clientes/procesarActualizacion" method="post" enctype="multipart/form-data">
<input type="hidden" name="id_cli"  id="id_cli" value="<?php echo $cliente->id_cli; ?>">
<br>
  <div class="col-12 grid-margin">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">INGRESAR DATOS DE LA FACTURA</h4>
        <form class="form-sample">
          <p class="card-description"> Ingrese todos los campos vacios </p>
          <br>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-sm-3 col-form-label">CEDULA:</label>
                <div class="col-sm-9">
                  <select class="form-control" name="fk_id_cli" id="fk_id_cli" required>
                       <option value="">----Seleccione un Cliente-----</option>
                             <?php if ($listadoClientes): ?>
                             <?php foreach ($listadoClientes->result() as $paisTemporal): ?>
                             <option value="<?php echo $clienteTemporal->id_cli; ?>"><?php echo $clienteTemporal->nombre_cli; ?></option>
                             <?php endforeach; ?>
                            <?php endif; ?>
                  </select>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-sm-3 col-form-label">FECHA FACTURA:</label>
                <div class="col-sm-9">
                  <input type="text" value="<?php echo $cliente->fecha_factura; ?>" name="fecha_factura" id="fecha_factura"  class="form-control" />
                </div>
              </div>
            </div>
          </div>
          <br>

          <br>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group row">
                <button type="submit" class="btn btn-dark btn-lg" align="center"><i class="fa fa-save"></i>&nbsp;GUARDAR</button>
                &nbsp;&nbsp;&nbsp;
                  <a href="<?php echo site_url('clientes/index'); ?>"class="btn btn-danger btn-lg" align="center"><i class="fa fa-times"></i>&nbsp;CANCELAR</a>
              </div>
            </div>
          </div>
              <input type="range" name="" value="">
        </form>
      </div>
    </div>
  </div>
</form>
<script type="text/javascript">
// Activado el pais seleccionado para el cliente
$("#fk_id_cli").val("<?php  echo $cliente->fk_id_cli; ?>");

</script>
<script type="text/javascript">
  $("#foto_cli").fileinput({
    allowedFileExtension:["jpeg","jpg","png"],
    dropZoneEnabled:true,
    language:"es"
  });
</script>
