<br>
<center>
  <ul class="nav nav-tabs">
    <li class="nav-item">
      <a class="nav-link active" aria-current="page" href="<?php echo site_url('facturas/index'); ?>">LISTADO DE   FACTURA</a>
    </li>
    <li class="nav-item">
      <a class="nav-link active" aria-current="page" href="<?php echo site_url('facturas/nuevo'); ?>">AGREGAR NUEVA FACTURA</a>
    </li>
  </ul>
</center>
<br>
<form  action="<?php echo site_url(); ?>/facturas/guardarFactura" method="post" id="frm_nuevo_factura"  enctype="multipart/form-data">
  <div class="col-12 grid-margin">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">INGRESAR DATOS DE LA FACTURA</h4>
        <form class="form-sample">
          <p class="card-description"> Ingrese todos los campos vacios </p>
          <br>
          <div class="row">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">CEDULA:</label>
              <div class="col-sm-9">
                <select class="form-control" name="fk_id_cli" id="fk_id_cli" required>
                     <option value="">----Seleccione un Cliente-----</option>
                           <?php if ($listadoClientes): ?>
                           <?php foreach ($listadoClientes->result() as $paisTemporal): ?>
                           <option value="<?php echo $clienteTemporal->id_cli; ?>"><?php echo $clienteTemporal->nombre_cli; ?></option>
                           <?php endforeach; ?>
                          <?php endif; ?>
                </select>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-sm-3 col-form-label">FECHA FACTURA:</label>
                <div class="col-sm-9">
                  <input type="text" name="fecha_factura" id="fecha_factura"  class="form-control" />
                </div>
              </div>
            </div>
          </div>
          <br>
          <br>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group row">
                <button type="submit" class="btn btn-dark btn-lg" align="center"><i class="fa fa-save"></i>&nbsp;GUARDAR</button>
                &nbsp;&nbsp;&nbsp;
                  <a href="<?php echo site_url('facturas/index'); ?>"class="btn btn-danger btn-lg" align="center"><i class="fa fa-times"></i>&nbsp;CANCELAR</a>
              </div>
            </div>
          </div>
              <input type="range" name="" value="">
        </form>
      </div>
    </div>
  </div>
</form>
<script type="text/javascript">
  $('#frm_nuevo_factura').validate({
      rules:{
        fk_id_cli:{
          required:true,

        },
        fecha_factura:{
          required:true,
        }



      },
      messages:{
        fk_id_cli:{
          required:"Por favor ingrese el cliente",

        },
        fecha_cli:{
          required:true
        }
      }

  });
</script>
</script>
<script type="text/javascript">
  $("#foto_cli").fileinput({
    allowedFileExtension:["jpeg","jpg","png"],
    dropZoneEnabled:true,
    language:"es"
  });
</script>
