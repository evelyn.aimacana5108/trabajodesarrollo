<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css"
crossorigin="anonymous">
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<link href="vendor/fontawesome/css/all.css" rel="stylesheet">
<link href="assets/css/style.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-login-form/1.0.0/bootstrap-login-form.min.css" integrity="sha512-Dzi0zz9zCe2olZNhq+wnzGjO5ILOv8f/yD6j8srW+XGnnv9dUN04eEoIdVHxQqiy8uBn21niIWQpiCzYJEH3yg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
</head>
<body>
  <div class="d-flex flex-column min-vh-100 justify-content-center align-items-center"
  id="template-bg-3">

  <div class="card mb-5 p-5 bg-dark bg-gradient text-white col-md-4">
  <div class="card-header text-center">
  <h3><i class="fas fa-user mt-2"></i>Iniciar sesión </h3>
  </div>
  <div class="card-body mt-3">
    <form class="pt-3" action="<?php echo site_url(); ?>/seguridades/validarAcceso" method="post">
                    <div class="form-group">
                      <input type="email" class="form-control form-control-lg" name="email_usu" id="email_usu" placeholder="Email">
                    </div>
                    <br>
                    <div class="form-group">
                      <label for="">PASSWORD</label>
                      <input type="password" class="form-control form-control-lg"name="password_usu" id="password_usu" placeholder="Password">
                    </div>
                    <div class="mt-3">
                      <button type="submit" class="btn btn-primary btn-info btn-lg font-weight-medium auth-form-btn"> Ingresar </button>

                    </div>
                    <br>
                  </form>
                  <form class="" action="<?php echo site_url("seguridades/recuperarPassword") ?>" method="post">
                    <div class="">
                      <h4 align="center">Ha olvidado su contraseña</h4>
                    </div>
                    <div class="form-group">

                      <input type="email" class="form-control form-control-lg" name="email" id="email" placeholder="Email">
                      <br>
                      <button type="submit" class="btn btn-block btn btn-danger btn-lg font-weight-medium auth-form-btn">RECUPERAR</button>

                    </div>
<?php if ($this->session->flashdata("error")):?>
  <script type="text/javascript">
alert("<?php echo $this->session->flashdata("error");?>");
  </script>
  <?php endif;?>

<?php if ($this->session->flashdata("confirmacion")):?> ?>
  <script type="text/javascript">
    alert("<?php echo $this->session->flashdata("confirmacion");?>");
  </script>
<?php endif; ?>

  </form>
  <?php if(!empty($loginResult)){?>
  <div class="text-danger"><?php echo $loginResult;?></div>
  <?php }?>
  </div>
  <div class="card-footer p-3">
  <div class="d-flex justify-content-center">
  <div class="text-primary">Si eres un usuario registrado, inicia sesión aquí.</div>
  </div>
  </div>
  </div>

  </div>
</body>
</html>
<!-- //////////////////////////////// -->
 <br>
 <!-- <form class="" action="<?php echo site_url("seguridades/recuperarPassword"); ?>" method="post" align="center">
   <h3><i class="fa fa-times">RECUPERAR CONTRASEÑA</i></h3>
   <label for="">Ingrese su Email</label>
   <input type="email" name="email" value="">
   <button type="submit" name="button">RECUPERAR AHORA</button>
 </form> -->

 <!-- //////////////////////////////////////////////////////////////////////// -->
