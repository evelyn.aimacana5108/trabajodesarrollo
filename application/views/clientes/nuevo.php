<br>
<center>
  <ul class="nav nav-tabs">
    <li class="nav-item">
      <a class="nav-link active" aria-current="page" href="<?php echo site_url('clientes/index'); ?>">LISTADO DE CLIENTES</a>
    </li>
    <li class="nav-item">
      <a class="nav-link active" aria-current="page" href="<?php echo site_url('clientes/nuevo'); ?>">AGREGAR NUEVO CLIENTE</a>
    </li>
  </ul>
</center>
<br>
<form  action="<?php echo site_url(); ?>/clientes/guardarCliente" method="post" id="frm_nuevo_cliente"  enctype="multipart/form-data">
  <div class="col-12 grid-margin">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">INGRESAR DATOS DEL CLIENTE</h4>
        <form class="form-sample">
          <p class="card-description"> Ingrese todos los campos vacios </p>
          <br>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-sm-3 col-form-label">CEDULA:</label>
                <div class="col-sm-9">
                  <input type="number" name="cedula_cli" id="cedula_cli" class="form-control" />
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-sm-3 col-form-label">APELLIDOS:</label>
                <div class="col-sm-9">
                  <input type="text" name="apellido_cli" id="apellido_cli"  class="form-control" />
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-sm-3 col-form-label">NOMBRES: </label>
                <div class="col-sm-9">
                  <input type="text"  name="nombre_cli" id="nombre_cli" class="form-control" />
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-sm-3 col-form-label">CIUDAD: </label>
                <div class="col-sm-9">
                  <input type="text"  name="ciudad_cli" id="ciudad_cli" class="form-control" />
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
              <div class="form-group row">
                <label class="col-sm-3 col-form-label">DIRECCION: </label>
                <div class="col-sm-9">
                  <input type="text"  name="direccion_cli" id="direccion_cli" class="form-control" />
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group row">
                <label class="col-sm-3 col-form-label">TELEFONO</label>
                <div class="col-sm-9">
                  <input type="number"  name="telefono_cli" id="telefono_cli" class="form-control" placeholder="" />
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group row">
                <label class="col-sm-3 col-form-label">EMAIL</label>
                <div class="col-sm-9">
                  <input type="email"  name="email_cli" id="email_cli" class="form-control" placeholder="" />
                </div>
              </div>
            </div>
          </div>
          <br>
  <label for="">FOTOGRAFIA</label>
  <input type="file" name="foto_cli"
  accept="image/*"
  id="foto_cli" value="">
          <br>
          <br>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group row">
                <button type="submit" class="btn btn-dark btn-lg" align="center"><i class="fa fa-save"></i>&nbsp;GUARDAR</button>
                &nbsp;&nbsp;&nbsp;
                  <a href="<?php echo site_url('clientes/index'); ?>"class="btn btn-danger btn-lg" align="center"><i class="fa fa-times"></i>&nbsp;CANCELAR</a>
              </div>
            </div>
          </div>
              <input type="range" name="" value="">
        </form>
      </div>
    </div>
  </div>
</form>
<script type="text/javascript">
  $('#frm_nuevo_cliente').validate({
      rules:{
        cedula_cli:{
          required:true,
          minlength:10,
          maxlength:10,
          digits:true,
        },
        apellido_cli:{
          letras:true,
          required:true,



        },
        nombre_cli:{
          letras:true,
          required:true,

        },
        ciudad_cli:{
          required:true,
          minlength:9,
          maxlength:10,
          digits:true,

        },
        direccion_cli:{
          required:true,
        },
        telefono_cli:{
          number:true,
          required:true,
        },
        email_cli:{
          required:true,
          email:true,
        }



      },
      messages:{
        cedula_cli:{
          required:"Por favor ngrese la cedula",
          minlength:"La cedula debe tener 10 dijitos",
          maxlength:"la cedula debe tener 10 numeros",
          digits:"Solo acepta numeros"
        },
        apellido_cli:{
          letras:"Apellido incorrecto",
          required:"Por favo ingrese el apellido",
        },
        nombre_cli:{
          letras:"Nombre incorrecto",
          required:"Por favo ingrese el Nombre"
        },
        ciudad_cli:{
          required:"Por favo ingrese la ciudad"
        },
        direccion_cli:{
          required:"Solo acepta direcciones validas",
        },
        telefono_cli:{
          number:" Error numero de Telefono"
          required:"Ingrese la direccion",
        },
        email_cli:{
          required:"Solo acepta correos electronicos validos",
          email:"Correro incorrecto"
        }
      }

  });
</script>
</script>
<script type="text/javascript">
  $("#foto_cli").fileinput({
    allowedFileExtension:["jpeg","jpg","png"],
    dropZoneEnabled:true,
    language:"es"
  });
</script>
