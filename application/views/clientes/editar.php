<br>
<center>
  <ul class="nav nav-tabs">
    <li class="nav-item">
      <a class="nav-link active" aria-current="page" href="<?php echo site_url('clientes/index'); ?>">LISTADO DE CLIENTES</a>
    </li>
    <li class="nav-item">
      <a class="nav-link active" aria-current="page" href="<?php echo site_url('clientes/nuevo'); ?>">AGREGAR NUEVO CLIENTE</a>
    </li>
  </ul>
</center>
<br>
<form  action="<?php echo site_url(); ?>/clientes/procesarActualizacion" method="post" enctype="multipart/form-data">
<input type="hidden" name="id_cli"  id="id_cli" value="<?php echo $cliente->id_cli; ?>">
<br>
  <div class="col-12 grid-margin">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">INGRESAR DATOS DEL CLIENTE</h4>
        <form class="form-sample">
          <p class="card-description"> Ingrese todos los campos vacios </p>
          <br>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-sm-3 col-form-label">CEDULA:</label>
                <div class="col-sm-9">
                  <input type="number" value="<?php echo $cliente->cedula_cli; ?>" name="cedula_cli" id="cedula_cli" class="form-control" />
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-sm-3 col-form-label">APELLIDOS:</label>
                <div class="col-sm-9">
                  <input type="text" value="<?php echo $cliente->apellido_cli; ?>" name="apellido_cli" id="apellido_cli"  class="form-control" />
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-sm-3 col-form-label">NOMBRES: </label>
                <div class="col-sm-9">
                  <input type="text" value="<?php echo $cliente->nombre_cli; ?>"  name="nombre_cli" id="nombre_cli" class="form-control" />
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group row">
                <label class="col-sm-3 col-form-label">CIUDAD: </label>
                <div class="col-sm-9">
                  <input type="text" value="<?php echo $cliente->ciudad_cli; ?>"  name="ciudad_cli" id="ciudad_cli" class="form-control" />
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
              <div class="form-group row">
                <label class="col-sm-3 col-form-label">DIRECCION: </label>
                <div class="col-sm-9">
                  <input type="text" value="<?php echo $cliente->direccion_cli; ?>"  name="direccion_cli" id="direccion_cli" class="form-control" />
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group row">
                <label class="col-sm-3 col-form-label">TELEFONO</label>
                <div class="col-sm-9">
                  <input type="number" value="<?php echo $cliente->telefono_cli; ?>"  name="telefono_cli" id="telefono_cli" class="form-control" placeholder="dd/mm/yyyy" />
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group row">
                <label class="col-sm-3 col-form-label">EMAIL</label>
                <div class="col-sm-9">
                  <input type="email" value="<?php echo $cliente->email_cli; ?>"  name="email_cli" id="email_cli" class="form-control" placeholder="dd/mm/yyyy" />
                </div>
              </div>
            </div>
          </div>
          <br>
  <label for="">FOTOGRAFIA</label>
  <input type="file" name="foto_cli"
  accept="image/*"
  id="foto_cli" value="">
          <br>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group row">
                <button type="submit" class="btn btn-dark btn-lg" align="center"><i class="fa fa-save"></i>&nbsp;GUARDAR</button>
                &nbsp;&nbsp;&nbsp;
                  <a href="<?php echo site_url('clientes/index'); ?>"class="btn btn-danger btn-lg" align="center"><i class="fa fa-times"></i>&nbsp;CANCELAR</a>
              </div>
            </div>
          </div>
              <input type="range" name="" value="">
        </form>
      </div>
    </div>
  </div>
</form>
<script type="text/javascript">
// Activado el pais seleccionado para el cliente
// $("#fk_id_pais").val("<?php  echo $cliente->fk_id_pais; ?>");
// $("#estado_cli").val("<?php  echo $cliente->estado_cli; ?>");

</script>
<script type="text/javascript">
  $("#foto_cli").fileinput({
    allowedFileExtension:["jpeg","jpg","png"],
    dropZoneEnabled:true,
    language:"es"
  });
</script>
