  <br>
  <center>
    <ul class="nav nav-tabs">
      <li class="nav-item">
        <a class="nav-link active" aria-current="page" href="<?php echo site_url('clientes/index'); ?>">LISTADO DE CLIENTES</a>
      </li>
      <li class="nav-item">
        <a class="nav-link active" aria-current="page" href="<?php echo site_url('clientes/nuevo'); ?>">AGREGAR NUEVO CLIENTE</a>
      </li>
    </ul>
  </center>
  <hr>
  <?php if ($listadoClientes): ?>
    <table class="table table-bordered table-striped table-hover" id="tbl-clientes">
      <thead class="table-dark">
        <tr>
          <th class="text-center">ID</th>
          <th class="text-center">FOTO</th>
          <th class="text-center">CEDULA</th>
          <th class="text-center">APELLIDOS</th>
          <th class="text-center">NOMBRES</th>
          <th class="text-center">CIUDAD</th>
          <th class="text-center">DIRECCION</th>
          <th class="text-center">TELEFONO</th>
          <th class="text-center">EMAIL</th>
          <th class="text-center">OPCIONES</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($listadoClientes->result() as $filaTemporal): ?>
          <tr>
            <td class="text-center"><?php echo $filaTemporal->id_cli;?></td>
            <td class="text-center">
                                 <?php if ($filaTemporal->foto_cli!=""): ?>
                                   <img
                                   src="<?php echo base_url(); ?>/uploads/clientes/<?php echo $filaTemporal->foto_cli; ?>"
                                   height="80px"
                                   width="100px"
                                   alt="">
                                 <?php else: ?>
                                   N/A
                                 <?php endif; ?>
                               </td>
            <td class="text-center"><?php echo $filaTemporal->cedula_cli;?></td>
            <td class="text-center"><?php echo $filaTemporal->apellido_cli;?></td>
            <td class="text-center"><?php echo $filaTemporal->nombre_cli;?></td>
            <td class="text-center"><?php echo $filaTemporal->ciudad_cli;?></td>
            <td class="text-center"><?php echo $filaTemporal->direccion_cli;?></td>
            <td class="text-center"><?php echo $filaTemporal->telefono_cli;?></td>
            <td class="text-center"><?php echo $filaTemporal->email_cli;?></td>
            <td class="text-center">
                <a href="<?php echo site_url(); ?>/Clientes/editar/<?php echo $filaTemporal->id_cli;?>"class="btn btn-warning"><i class="fa fa-pen"></i></a>
                <?php if ($this->session->userdata("c0nectadoUSU")->perfil_usu=="ADMINISTRADOR"): ?>
                  <a onclick="confirmarEliminacion('<?php echo $filaTemporal->id_cli; ?>')" href="javascript:void(0)" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                <?php else: ?>

                <?php endif; ?>

              <!-- return confirm('¿Estas seguro de eliminar'); -->
                <!-- <?php echo site_url(); ?>/clientes/procesarEliminacion/<?php echo $filaTemporal->id_cli; ?> -->


              </td>
                      </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              <?php else: ?>
                <div class="alert alert-danger">
                  <h3>No se encontraron clientes resgistrados</h3>
                </div>
              <?php endif; ?>

              <script type="text/javascript">
                  function confirmarEliminacion(id_cli){
                        iziToast.question({
                            timeout: 20000,
                            close: false,
                            overlay: true,
                            displayMode: 'once',
                            id: 'question',
                            zindex: 999,
                            title: 'CONFIRMACIÓN',
                            message: '¿Esta seguro de eliminar el cliente de forma pernante?',
                            position: 'center',
                            buttons: [
                                ['<button><b>SI</b></button>', function (instance, toast) {

                                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                                    window.location.href=
                                    "<?php echo site_url(); ?>/clientes/procesarEliminacion/"+id_cli;

                                }, true],
                                ['<button>NO</button>', function (instance, toast) {

                                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                                }],
                            ]
                        });
                  }
              </script>
              <script type="text/javascript">
            $(document).ready(function() {
                $('#tbl-clientes').DataTable( {
                  dom: 'Blfrtip',
                  buttons: [
                      'copyHtml5',
                      'excelHtml5',
                      'csvHtml5',
                      'pdfHtml5'
                  ],
                    "order": [[ 3, "desc" ]],
                  language: {
                    "decimal":        "",
                "emptyTable":     "No hay datos",
                "info":           "Mostrando START a END de TOTAL registros",
                "infoEmpty":      "Mostrando 0 a 0 de 0 registros",
                "infoFiltered":   "(Filtro de MAX total registros)",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     "Mostrar MENU registros",
                "loadingRecords": "Cargando...",
                "processing":     "Procesando...",
                "search":         "Buscar:",
                "zeroRecords":    "No se encontraron coincidencias",
                "paginate": {
                    "first":      "Primero",
                    "last":       "Ultimo",
                    "next":       "Próximo",
                    "previous":   "Anterior"
                },
                "aria": {
                    "sortAscending":  ": Activar orden de columna ascendente",
                    "sortDescending": ": Activar orden de columna desendente"
                }
                  }
                } );
            } );

            </script>
