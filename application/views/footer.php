<script type="text/javascript" src="//cdn.datatables.net/1.11.4/js/jquery.dataTables.min.js"> </script>
      <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.2/js/dataTables.buttons.min.js"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
      <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.html5.min.js"></script>
<!-- gggggggggggggggggggggggggggggg -->



<div class="footer-wrap pd-20 mb-20 card-box">
				DeskApp - Bootstrap 4 Admin Template By <a href="https://github.com/dropways" target="_blank">Ankit Hingarajiya</a>
			</div>
		</div>
	</div>
	<!-- js -->
	<script src="<?php echo base_url(); ?>/assets/vendors/scripts/core.js"></script>
	<script src="<?php echo base_url(); ?>/assets/vendors/scripts/script.min.js"></script>
	<script src="<?php echo base_url(); ?>/assets/vendors/scripts/process.js"></script>
	<script src="<?php echo base_url(); ?>/assets/vendors/scripts/layout-settings.js"></script>
	<script src="<?php echo base_url(); ?>/assets/src/plugins/apexcharts/apexcharts.min.js"></script>
	<script src="<?php echo base_url(); ?>/assets/src/plugins/datatables/js/jquery.dataTables.min.js"></script>
	<script src="<?php echo base_url(); ?>/assets/src/plugins/datatables/js/dataTables.bootstrap4.min.js"></script>
	<script src="<?php echo base_url(); ?>/assets/src/plugins/datatables/js/dataTables.responsive.min.js"></script>
	<script src="<?php echo base_url(); ?>/assets/src/plugins/datatables/js/responsive.bootstrap4.min.js"></script>
	<script src="<?php echo base_url(); ?>/assets/vendors/scripts/dashboard.js"></script>
	<script src="<?php echo base_url();?>/assests/js/dashboard.js"></script>

  <?php if ($this->session->flashdata('confirmacion')): ?>
    <script type="text/javascript">
      iziToast.success({
        title: 'CONFIRMACION',
        message: '<?php echo $this->session->flashdata('confirmacion'); ?>',
        position: 'topRight',
      });
    </script>
  <?php endif; ?>
  <?php if ($this->session->flashdata('error')): ?>
   <script type="text/javascript">
     iziToast.danger({
       title: 'ADVERTENCIA',
       message: '<?php echo $this->session->flashdata('error'); ?>',
       position: 'topRight',
     });
   </script>
  <?php endif; ?>

  <?php if ($this->session->flashdata("bienvenida")): ?>
   <script type="text/javascript">
     iziToast.info({
          title: 'CONFIRMACIÓN',
          message: '<?php echo $this->session->flashdata("bienvenida"); ?>',
          position: 'topRight',
        });
   </script>
 <?php endif; ?>

  <!-- End custom js for this page-->
  <style media="screen">
      .error{
        color:red;
        font-size: 16px;
      }
      input.error, select.error{
        border: 2px solid red;
      }
  </style>


 </body>

 </html>
