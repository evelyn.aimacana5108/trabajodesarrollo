<?php
    class Facturas extends CI_Controller{
        public function __construct(){
          parent::__construct();
          $this->load->model('factura');
          $this->load->model("cliente");
          if ($this->session->userdata("c0nectadoUSU")) {
            // code...
          }else{
            redirect("seguridades/formularioLogin");
          }
        }

        public function index(){
          $data["listadoFacturas"]=$this->factura->consultarTodos();
          $this->load->view('header');
          $this->load->view('facturas/index',$data);
          $this->load->view('footer');
        }

        public function nuevo(){
          $data["listadoClientes"]=$this->cliente->consultarTodos();
          $this->load->view('header');
          $this->load->view('facturas/nuevo',$data);
          $this->load->view('footer');
        }
        public function editar($id_factura){
          $data["listadoClientes"]=$this->cliente->consultarTodos();
          $data["factura"]=$this->factura->consultarPorId($id_cli);
          $this->load->view('header');
          $this->load->view('facturas/editar',$data);
          $this->load->view('footer');
        }
        public function procesarActualizacion(){
          $id_factura=$this->input->post("id_factura");
          $datosFacturaEditado=array(
            'fecha_factura'=>$this->input->post('fecha_factura'),
            "fk_id_cli"=>$this->input->post("fk_id_cli"),
          );
          if ($this->factura->actualizar($id_factura,$datosFacturaEditado)) {
            //echo "INSERCION EXITOSA";
            redirect("facturas/index");
          } else {
            echo "ERROR AL ACTUALIZAR";
          }
        }
        //registro de usuarios
        public function guardarFactura(){
          $datosNuevoFactura=array(
            'fecha_factura'=>$this->input->post('fecha_factura'),
            "fk_id_cli"=>$this->input->post("fk_id_cli"),
          );
          if ($this->factura->insertar($datosNuevoFactura)) {
              $this->session->set_flashdata("confirmacion","FACTURA INSERTADO EXITOSAMENTE.");
          } else {
            $this->session->set_flashdata("error","ERROR AL PROCESAR, INTENTE NUEVAMENTE.");
          }
          redirect("facturas/index");

        }
        function procesarEliminacion($id_factura){
          if ($this->session->userdata("c0nectadoUSU")->perfil_usu=="ADMINISTRADOR") {
            // code...
            if($this->factura->eliminar($id_factura)){
              redirect("facturas/index");
            }else{
              echo "Error al eliminar";
            }
          }else{
            redirect("seguridades/formularioLogin");
          }
    }
    }//cierre de la clase
 ?>
