<?php
    class Clientes extends CI_Controller{
        public function __construct(){
          parent::__construct();
          $this->load->model('cliente');
          if ($this->session->userdata("c0nectadoUSU")) {
            // code...
          }else{
            redirect("seguridades/formularioLogin");
          }
        }

        public function index(){
          $data["listadoClientes"]=$this->cliente->consultarTodos();
          $this->load->view('header');
          $this->load->view('clientes/index',$data);
          $this->load->view('footer');
        }

        public function nuevo(){
          $this->load->view('header');
          $this->load->view('clientes/nuevo');
          $this->load->view('footer');
        }
        public function editar($id_cli){
          $data["cliente"]=$this->cliente->consultarPorId($id_cli);
          $this->load->view('header');
          $this->load->view('clientes/editar',$data);
          $this->load->view('footer');
        }
        public function procesarActualizacion(){
          $id_cli=$this->input->post("id_cli");
          $datosClienteEditado=array(
            'cedula_cli'=>$this->input->post('cedula_cli'),
            'apellido_cli'=>$this->input->post('apellido_cli'),
            'nombre_cli'=>$this->input->post('nombre_cli'),
            'pais_cli'=>$this->input->post('pais_cli'),
            'ciudad_cli'=>$this->input->post('ciudad_cli'),
            'direccion_cli'=>$this->input->post('direccion_cli'),
            'telefono_cli'=>$this->input->post('telefono_cli'),
            'email_cli'=>$this->input->post('email_cli'),
          );
          $this->load->library("upload");
          $new_name = "foto_cliente_" . time() . "_" . rand(1, 5000);
          $config["file_name"]=$nombreTemporal;
          $config["upload_path"]=APPPATH.'../uploads/clientes/';
          $config['allowed_types']        = 'jpeg|jpg|png';
          $config['max_size']             = 4*1024;
          $this->upload->initialize($config);

          if ($this->upload->do_upload("foto_cli")) {
            $dataSubida = $this->upload->data();
            $datosClienteEditado["foto_cli"] = $dataSubida['file_name'];
          }

          if ($this->cliente->actualizar($id_cli,$datosClienteEditado)) {
            //echo "INSERCION EXITOSA";
            redirect("clientes/index");
          } else {
            echo "ERROR AL ACTUALIZAR";
          }
        }
        //registro de usuarios
        public function guardarCliente(){
          $datosNuevoCliente=array(
            'cedula_cli'=>$this->input->post('cedula_cli'),
            'apellido_cli'=>$this->input->post('apellido_cli'),
            'nombre_cli'=>$this->input->post('nombre_cli'),
            'pais_cli'=>$this->input->post('pais_cli'),
            'ciudad_cli'=>$this->input->post('ciudad_cli'),
            'direccion_cli'=>$this->input->post('direccion_cli'),
            'telefono_cli'=>$this->input->post('telefono_cli'),
            'email_cli'=>$this->input->post('email_cli'),
          );
          //logiga de negocio para subir la foto del cliente
          $this->load->library("upload");//carga de la libreria de subida de archivos
          $nombreTemporal="foto_cliente_".time()."_".rand(1,5000);
          $config["file_name"]=$nombreTemporal;
          $config["upload_path"]=APPPATH.'../uploads/clientes/';
          $config["allowed_types"]="jpeg|jpg|png";
          $config["max_size"]=2*1024;//2mb
          $this->upload->initialize($config);
          //codigo para subir el archivo y guradar el nombre en la BBD
          if ($this->upload->do_upload("foto_cli")) {
            $dataSubida=$this->upload->data();
            $datosNuevoCliente["foto_cli"]=$dataSubida["file_name"];
          }

          if ($this->cliente->insertar($datosNuevoCliente)) {
              $this->session->set_flashdata("confirmacion","CLIENTE INSERTADO EXITOSAMENTE.");
          } else {
            $this->session->set_flashdata("error","ERROR AL PROCESAR, INTENTE NUEVAMENTE.");
          }
          redirect("clientes/index");

        }
        function procesarEliminacion($id_cli){
          if ($this->session->userdata("c0nectadoUSU")->perfil_usu=="ADMINISTRADOR") {
            // code...
            if($this->cliente->eliminar($id_cli)){
              redirect("clientes/index");
            }else{
              echo "Error al eliminar";
            }
          }else{
            redirect("seguridades/formularioLogin");
          }
    }
    }//cierre de la clase
 ?>
