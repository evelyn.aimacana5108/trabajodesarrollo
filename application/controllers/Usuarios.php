<?php
    class Usuarios extends CI_Controller{
        public function __construct(){
          parent::__construct();
          $this->load->model('usuario');
          // validacion si alguien esta conectado
          if ($this->session->userdata("c0nectadoUSU")) {
            // code...
            if ($this->session->userdata("c0nectadoUSU")->perfil_usu=="ADMINISTRADOR") {
              // Si es ADMINISTRADOR
            }else {
              redirect("/");
            }
          }else{
            redirect("seguridades/formularioLogin");
          }
        }
        public function index(){
          $this->load->view('header');
          $this->load->view('usuarios/index');
          $this->load->view('footer');
        }

        public function listado(){
          $data["listadoUsuarios"]=$this->usuario->obtenerTodos();
          $this->load->view('usuarios/listado',$data);
        }

        public function insertarUsuario(){
          $data=array(
            "apellido_usu"=>$this->input->post("apellido_usu"),
            "nombre_usu"=>$this->input->post("nombre_usu"),
            "email_usu"=>$this->input->post("email_usu"),
            "password_usu"=>$this->input->post("password_usu"),
            "perfil_usu"=>$this->input->post("perfil_usu"),
          );
          if ($this->usuario->insertarUsuario($data)){
            // code...
            $this->session->set_flashdata("respuesta","ok");
            // echo json_encode(array("respuesta"=>"ok"));
          } else {
            // code...
            $this->session->set_flashdata("respuesta","error");
            // echo json_encode(array("respuesta"=>"error"));
          }
          redirect("usuarios/index");
        }

        public function eliminarUsuario(){
          $id_usu=$this->input->post("id_usu");
          if ($this->usuario->eliminar($id_usu)){
            echo json_encode(array("respuesta"=>"ok"));
        }else{
          echo json_encode(array("respuesta"=>"error"));
        }
      }
// FORMATO AJAX CUANDO NO SE CARGA HEADER Y FOOTER
      public function editar($id_usu){
        $data["usuario"]=$this->usuario->obtenerPorId($id_usu);
        //cargar la vista
        $this->load->view("usuarios/editar",$data);
      }

      public function actualizarUsuarioAjax(){
          $id_usu=$this->input->post("id_usu");
          $data=array(
              "apellido_usu"=>$this->input->post("apellido_usu"),
              "nombre_usu"=>$this->input->post("nombre_usu"),
              "email_usu"=>$this->input->post("email_usu"),
              "perfil_usu"=>$this->input->post("perfil_usu")
          );
          if($this->usuario->actualizar($data,$id_usu)){
              echo json_encode(array("respuesta"=>"ok"));
          }else{
              echo json_encode(array("respuesta"=>"error"));
          }
      }

    }//cierre de la clase
 ?>
