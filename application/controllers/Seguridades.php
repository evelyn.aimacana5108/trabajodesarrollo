<?php
  class Seguridades extends CI_Controller{
    function __construct(){
      parent::__construct();
      $this->load->model("usuario");
    }
    //funcion para renderizar
    //la vista con el formulario de login
    public function formularioLogin(){
      $this->load->view("seguridades/formularioLogin");
    }
    //funcion validar credenciales ingresadas
    public function validarAcceso(){
      $email_usu=$this->input->post("email_usu");
      $password_usu=$this->input->post("password_usu");
      $usuario=$this->usuario->buscarUsuarioPorEmailPassword($email_usu,$password_usu);
      if ($usuario) {
        // credenciales ingresadas correctas
        if ($usuario->estado_usu>0) {
          //creando variable de session con el nombre c0nectadoUSU -> nombre robusto
          $this->session->set_userdata("c0nectadoUSU",$usuario);
          $this->session->set_flashdata("bienvenida","saludos, Bienvenido al sistema");
          redirect("clientes/index");
        } else {
          $this->session->set_flashdata("error","Usuario bloqueado");
          redirect("seguridades/formularioLogin");
        }

      } else {
        //credenciales ingresadas incorrectas
        $this->session->set_flashdata("error","Email o Contraseña Incorrectos");
        redirect("seguridades/formularioLogin");
      }

    }

    public function cerrarSesion(){
      $this->session->set_flashdata("salir","Salir, ");
      $this->session->sess_destroy();
      redirect("seguridades/formularioLogin");
    }

    // para enviar correeos electronicos

    public function pruebaEmail(){
      enviarEmail("evelyn.984969054@gmail.com","REVISION","<h1>JACKELINE AIMACANA</h1>");
    }

    public function recuperarPassword(){
      $email=$this->input->post("email");
      $password_aleatorio=rand(111111,999999);
      $asunto="RECUPERAR PASSWORD";
      $contenido="SU contraseña temporal es: <b> $password_aleatorio</b>";
      enviarEmail($email,$asunto,$contenido);
      $this->session->set_flashdata("confirmacion","hemos enviado una clave a su direccion de email");
      redirect("seguridades/formularioLogin");
    }
// // PROCESO
// public function index(){
//   $this->load->view('header');
//   $this->load->view('usuarios/index');
//   $this->load->view('footer');
// }
//
// public function listado(){
//   $data["listadoUsuarios"]=$this->usuario->obtenerTodos();
//   $this->load->view('usuarios/listado',$data);
// }
//
// public function insertarUsuario(){
//   $data=array(
//     "apellido_usu"=>$this->input->post("apellido_usu"),
//     "nombre_usu"=>$this->input->post("nombre_usu"),
//     "email_usu"=>$this->input->post("email_usu"),
//     "password_usu"=>$this->input->post("password_usu"),
//     "perfil_usu"=>$this->input->post("perfil_usu"),
//   );
//   if ($this->usuario->insertarUsuario($data)){
//     // code...
//     $this->session->set_flashdata("respuesta","ok");
//     // echo json_encode(array("respuesta"=>"ok"));
//   } else {
//     // code...
//     $this->session->set_flashdata("respuesta","error");
//     // echo json_encode(array("respuesta"=>"error"));
//   }
//   redirect("usuarios/index");
// }
//
// public function eliminarUsuario(){
//   $id_usu=$this->input->post("id_usu");
//   if ($this->usuario->eliminar($id_usu)){
//     echo json_encode(array("respuesta"=>"ok"));
// }else{
//   echo json_encode(array("respuesta"=>"error"));
// }
// }
// // FORMATO AJAX CUANDO NO SE CARGA HEADER Y FOOTER
// public function editar($id_usu){
// $data["usuario"]=$this->usuario->obtenerPorId($id_usu);
// //cargar la vista
// $this->load->view("usuarios/editar",$data);
// }
//
// public function actualizarUsuarioAjax(){
//   $id_usu=$this->input->post("id_usu");
//   $data=array(
//       "apellido_usu"=>$this->input->post("apellido_usu"),
//       "nombre_usu"=>$this->input->post("nombre_usu"),
//       "email_usu"=>$this->input->post("email_usu"),
//       "perfil_usu"=>$this->input->post("perfil_usu")
//   );
//   if($this->usuario->actualizar($data,$id_usu)){
//       echo json_encode(array("respuesta"=>"ok"));
//   }else{
//       echo json_encode(array("respuesta"=>"error"));
//   }
// }
//
// // FIN PROCESO
  } //fin llave

?>
