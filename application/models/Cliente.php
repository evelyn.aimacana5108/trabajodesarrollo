<?php

    //constructor
    class Cliente extends CI_Model{
      //funcion constructor
        public function __construct(){
            parent:: __construct();
        }

        //funcion para insetar datos
        public function insertar($datos){
            return $this->db->insert('cliente');
        }

        public function actualizar($id_cli,$datos){
          $this->db->where("id_cli",$id_cli);
            return $this->db->update("cliente");
        }

        public function consultarPorId($id_cli){
          $this->db->where("id_cli",$id_cli);
            $cliente=$this->db->get('cliente');
            if ($cliente->num_rows()>0) {
                // Cuando si hay clientes registrados
                return $cliente->row();
            } else {
                //cuando no hay Clientes
                return false;
            }

        }

        //funcion para consultar Clientes
        public function consultarTodos(){
            $listadoClientes=$this->db->get('cliente');
            if ($listadoClientes->num_rows()>0) {
                // Cuando si hay clientes registrados
                return $listadoClientes;
            } else {
                //cuando no hay Clientes
                return false;
            }
        }

        public function eliminar($id_cli){
        $this->db->where("id_cli",$id_cli);
        return $this->db->delete("cliente");
    }


    }

 ?>
