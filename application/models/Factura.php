<?php

    //constructor
    class Factura extends CI_Model{
      //funcion constructor
        public function __construct(){
            parent:: __construct();
        }

        //funcion para insetar datos
        public function insertar($datos){
            return $this->db->insert('factura',$datos);
        }

        public function actualizar($id_factura,$datos){
          $this->db->where("id_factura",$id_factura);
            return $this->db->update("factura",$datos);
        }

        public function consultarPorId($id_factura){
          $this->db->where("id_factura",$id_factura);
          $this->db->join("cliente","cliente.id_cli=factura.fk_id_cli");
            $factura=$this->db->get('factura');
            if ($factura->num_rows()>0) {
                // Cuando si hay facturas registrados
                return $factura->row();
            } else {
                //cuando no hay facturas
                return false;
            }

        }

        //funcion para consultar facturas
        public function consultarTodos(){
          $this->db->join("cliente","cliente.id_cli=factura.fk_id_cli");
            $listadoFacturas=$this->db->get('factura');
            if ($listadoFacturas->num_rows()>0) {
                // Cuando si hayoFacturas registrados
                return $listadoFacturas;
            } else {
                //cuando no hayoFacturas
                return false;
            }
        }

        public function eliminar($id_factura){
        $this->db->where("id_factura",$id_factura);
        return $this->db->delete("factura");
    }


    }

 ?>
