<?php
    //constructor
  class Usuario extends CI_Model{
    //funcion constructor
    public function __construct(){
      parent:: __construct();
    }
    public function buscarUsuarioPorEmailPassword($email_usu,$password_usu){
      $this->db->where("email_usu",$email_usu);
      $this->db->where("password_usu",$password_usu);
      $usuarioEncontrado=$this->db->get("usuario");
      if ($usuarioEncontrado->num_rows()>0) {
        return $usuarioEncontrado->row();
      } else {
        return false;
      }
    }
    public function insertarUsuario($data){//datos de controlador-->trae toda la informacion del usuario
      return $this->db->insert("usuario",$data);
    }
    public function obtenerTodos(){
      $this->db->order_by("apellido_usu","asc");
      $listado=$this->db->get("usuario");
      if($listado->num_rows()>0){
        return $listado;
      }else{
        return false;
      }
    }
// para crear la ventana modal
    public function obtenerPorId($id_usu){
      $this->db->where("id_usu",$id_usu);
      $usuario=$this->db->get("usuario");
      if ($usuario->num_rows()>0){
        return $usuario->row();
        // code...
      }else{
        return false;
      }
    }
    public function eliminar($id_usu){
      $this->db->where("id_usu",$id_usu);
      return $this->db->delete('usuario');
    }


    public function actualizar($data, $id_usu){
        $this->db->where("id_usu",$id_usu);
        return $this->db->update("usuario",$data);
    }
    public function obtenerPorEmail($email_usu){
  $this->db->where("email_usu",$email_usu);
  $usuario=$this->db->get("usuario");
  if ($usuario->num_rows()>0) {
    return $usuario->row();
  }else{
    return false;
  }

}
  }//cierre clase
?>
